<?php declare(strict_types=1);

namespace Mdfx\SlackLogger\Messenger;

class SlackMessenger
{

    public function __construct(
    	private string $webhookUrl
	)
	{
    }

    public function sendMessage(SlackMessage $message): void
	{
        $message = [
            'payload' => \Nette\Utils\Json::encode($message->getPayload()),
        ];


		$this->sendByCurl($message);
    }


    protected function sendByCurl(array $message): void
	{
        $c = \curl_init($this->webhookUrl);
        \curl_setopt($c, \CURLOPT_SSL_VERIFYPEER, false);
        \curl_setopt($c, \CURLOPT_POST, true);
        \curl_setopt($c, \CURLOPT_POSTFIELDS, $message);
        \curl_setopt($c, \CURLOPT_RETURNTRANSFER, true);
        $result = \curl_exec($c);
        \curl_close($c);

        if ($result === FALSE) {
        	throw new \Mdfx\Messenger\Exception\SlackException();
		}
    }

}
