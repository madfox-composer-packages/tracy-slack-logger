<?php declare(strict_types=1);

namespace Mdfx\SlackLogger\Messenger;

class SlackMessage
{

    public function __construct(
    	private string $name,
		private string $iconEmoji,
		private string $text,
	)
	{
    }


	/**
	 * @return array<string, string>
	 */
    public function getPayload(): array
	{
        return [
        	'username' => $this->name,
			'icon_emoji' => $this->iconEmoji ? $this->parseEmoji($this->iconEmoji) : NULL,
			'text' => $this->text,
		];
    }


    private function parseEmoji(string $emoji): string
	{
		return \substr($emoji, 0, 1) !== ':' && \substr($emoji, -1) !== ':'
			? ":$emoji:"
			: $emoji
		;
	}

}
