<?php declare(strict_types=1);

namespace Mdfx\SlackLogger;

class TracyLogger extends \Tracy\Logger
{

	private \Mdfx\SlackLogger\Messenger\SlackMessenger $messenger;

    private array $reportedPriorities = [
    	\Tracy\ILogger::ERROR,
		\Tracy\ILogger::CRITICAL,
		\Tracy\ILogger::EXCEPTION
	];

	/**
	 * @var array<string>
	 */
    private array $ignoredUrls = [
    	'localhost',
		'127.0.0.1',
	];

	private string $slackIconEmoji = 'hammer_and_pick';

	private string $slackUsername = 'Error reporter';

	private bool $showStackTrace = FALSE;


    public function __construct(
    	private string $webhookURL,
		private bool $useDefaultLogger = TRUE,
	)
    {
        parent::__construct(
        	\Tracy\Debugger::$logDirectory,
			\Tracy\Debugger::$email,
			\Tracy\Debugger::getBlueScreen(),
		);

        $this->messenger = new \Mdfx\SlackLogger\Messenger\SlackMessenger($webhookURL);
    }

    public function addReportingLevel(string $level): void
    {
        $this->reportedPriorities = \array_unique([...$this->reportedPriorities, $level]);
    }


    public function setReportingLevels(array $levels): void
    {
        $this->reportedPriorities = $levels;
    }


    public function setSlackIconEmoji(string $emoji): void
    {
        $this->slackIconEmoji = $emoji;
    }


    public function setSlackUsername(string $slackUsername): void
    {
        $this->slackUsername = $slackUsername;
    }


    public function setShowStackTrace(bool $showStackTrace): void
	{
		$this->showStackTrace = $showStackTrace;
	}

	/**
	 * @param array<string> $ignoredUrls
	 */
	public function setIgnoredUrls(array $ignoredUrls): void
	{
		$this->ignoredUrls = $ignoredUrls;
	}


    function log($value, $priority = self::INFO): void
    {
		if ($this->useDefaultLogger) {
			parent::log($value, $priority);
		}

		if ( ! \in_array($priority, $this->reportedPriorities, TRUE)) {
			return;
		}

		if ($value instanceof \Nette\Application\BadRequestException) {
			return;
		}

        if (\is_array($value)) {
            $value = \implode(' ', $value);
        }

		$httpHost = $_SERVER['HTTP_HOST'] ?? NULL;

		foreach ($this->ignoredUrls as $ignoredUrl) {
			if (\str_contains($httpHost, $ignoredUrl)) {
				return;
			}
		}

        if ($httpHost) {
            $url = $httpHost . ($_SERVER['REQUEST_URI'] ?? '');
            $host = $httpHost;
        } else {
            $url = $_SERVER['argv'][0] ?? '';
            $host = 'CLI interpreter';
        }

        $sentences = ["*{$priority}* on *{$host}* (URL: $url):"];

        if (isset($_SERVER['REMOTE_ADDR'])) {
			$sentences[] = "*IP*: " . $_SERVER['REMOTE_ADDR'];
		}

        if (isset($_SERVER['HTTP_REFERER'])) {
			$sentences[] = "*Referer*: " . $_SERVER['HTTP_REFERER'];
		}

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$sentences[] = "*User agent*: " . $_SERVER['HTTP_USER_AGENT'];
		}

        if ($value instanceof \Exception && ! $this->showStackTrace) {
        	$description = $value->getMessage();
		} else {
        	$description = (string) $value;
		}

        $sentences[] = "*Description*:\n$description";

        $text = \implode("\n", $sentences);

        $message = new \Mdfx\SlackLogger\Messenger\SlackMessage(
        	$this->slackUsername,
			$this->slackIconEmoji,
			$text,
		);

        try {
            $this->messenger->sendMessage($message);
        } catch (\Mdfx\SlackLogger\Messenger\Exception\SlackException $exception) {
            throw $exception;
        }
    }
}
